package com.codingexercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.codingexercise.Card.Face;
import com.codingexercise.Card.Suit;

/**
 * The dealer class responsible for evaluating cards and concluding the game
 * with a result printed out.
 * 
 * @author anandjayaraman
 *
 */
public class Dealer {
	private final Player playerONE;
	private final Player playerTWO;

	private Map<Hand, EnumMap<Suit, Long>> suitGrouping;
	private Map<Hand, EnumMap<Face, Long>> faceGrouping;
	private Map<Hand, Map<String, Integer>> minMax;

	private static final Map<Player, Integer> results = new HashMap<>();

	private static class DealerRoster {
		private static final Dealer INSTANCE = new Dealer();
	}

	public static Dealer get() {
		return DealerRoster.INSTANCE;
	}

	private Dealer() {
		playerONE = new Player(1);
		playerTWO = new Player(2);

		results.put(playerONE, 0);
		results.put(playerTWO, 0);
	}

	/**
	 * Start each individual round, dealing cards to both players
	 * 
	 * @param game - a string representation of cards to be dealt to both players
	 */
	void deal(String game) {
		String[] hand = { game.substring(0, game.length() / 2), game.substring(game.length() / 2) };

		playerONE.hand(hand[0].trim());
		playerTWO.hand(hand[1].trim());

		initialiseGameParameters(playerONE.getHand(), playerTWO.getHand());
	}

	/**
	 * A utility method that helps evaluate the rank of each hand
	 * 
	 * @param h1
	 * @param h2
	 */
	private void initialiseGameParameters(Hand h1, Hand h2) {
		suitGrouping = new HashMap<>();
		suitGrouping.put(h1, suitMap(h1.getCards()));
		suitGrouping.put(h2, suitMap(h2.getCards()));

		suitGrouping = Collections.unmodifiableMap(suitGrouping);

		faceGrouping = new HashMap<>();
		faceGrouping.put(h1, faceMap(h1.getCards()));
		faceGrouping.put(h2, faceMap(h2.getCards()));

		faceGrouping = Collections.unmodifiableMap(faceGrouping);

		minMax = new HashMap<>();
		minMax.put(h1, computeMinMax(h1));
		minMax.put(h2, computeMinMax(h2));

		minMax = Collections.unmodifiableMap(minMax);
	}

	/**
	 * A utility method that retrieves the highest and lowest cards in a hand.
	 * Useful for many operations while evaluating the rank of a hand.
	 * 
	 * @param h
	 * @return a map with the min and max values of the given hand.
	 */
	private Map<String, Integer> computeMinMax(Hand h) {
		Map<String, Integer> map = new HashMap<>();
		Supplier<Stream<Face>> streamSupplier = () -> faceGrouping.get(h).entrySet().stream().map(e -> e.getKey());
		Face max = streamSupplier.get().max(Comparator.comparing(Face::getValue)).get();
		Face min = streamSupplier.get().min(Comparator.comparing(Face::getValue)).get();

		map.put("MIN", min.getValue());
		map.put("MAX", max.getValue());

		return Collections.unmodifiableMap(map);
	}

	/**
	 * Retrieve the grouping of individual faces of a card in the hand.
	 * 
	 * @param cards - the set of cards in the hand
	 * @return
	 */
	private EnumMap<Face, Long> faceMap(final Set<Card> cards) {
		return cards.stream()
				.collect(Collectors.groupingBy(Card::getFace, () -> new EnumMap<>(Face.class), Collectors.counting()));
	}

	/**
	 * Retrieve the grouping of individual suits in the hand.
	 * 
	 * @param cards - the set of cards in the hand.
	 * @return
	 */
	private EnumMap<Suit, Long> suitMap(final Set<Card> cards) {
		return cards.stream()
				.collect(Collectors.groupingBy(Card::getSuit, () -> new EnumMap<>(Suit.class), Collectors.counting()));
	}

	/**
	 * Evaluate each hand and attain a rank. Compare the rank values and handle ties
	 * accordingly.
	 */
	void show() {
		Hand p1Hand = playerONE.getHand();
		Hand p2Hand = playerTWO.getHand();

		HandRank p1Rank = evaluate(p1Hand);
		HandRank p2Rank = evaluate(p2Hand);

		if (p1Rank.getValue() > p2Rank.getValue()) {
			// playerONE wins
			results.put(playerONE, results.getOrDefault(playerONE, 0) + 1);
		} else if (p1Rank.getValue() < p2Rank.getValue()) {
			// playerTWO wins
			results.put(playerTWO, results.getOrDefault(playerTWO, 0) + 1);
		} else {
			// tie-breaker
			Entry<Player, Integer> tiebreakerResults = tiebreaker(p1Rank, p1Hand, p2Hand);
			if (tiebreakerResults == null) {
				// has to be a royal flush; cannot work out the winner
			} else {
				Player winner = tiebreakerResults.getKey();
				results.put(winner, results.getOrDefault(winner, 0) + 1);
			}
		}
	}

	/**
	 * Close the game and print the results. Results are printed in the following
	 * format:
	 * <p>
	 * <code> Player {number}: {number of wins}<code>
	 * </p>
	 */
	void closeGame() {
		System.out.println(String.format("%s: %d", playerONE, results.get(playerONE)));
		System.out.println(String.format("%s: %d", playerTWO, results.get(playerTWO)));
	}

	/**
	 * This determines the winner in the event where both players have the same
	 * ranked hand. For e.g.: if both players have a 3 of a kind, then determine the
	 * face of highest value and award the game to the player with that hand.
	 * 
	 * @param p1Rank - the tied rank each player has
	 * @param h1     - the hand of player one
	 * @param h2     - the hand of player two
	 * @return the eventual winner after resolving the tiebreak.
	 */
	private Entry<Player, Integer> tiebreaker(HandRank p1Rank, Hand h1, Hand h2) {
		Map<Player, Integer> tiebreaker = new HashMap<>();

		switch (p1Rank) {
		case PAIR: {
			tiebreaker = computeTieBreaker(2l, h1, h2);
		}
			break;
		case TWO_PAIRS: {
			tiebreaker = computeTieBreaker(2l, h1, h2);
		}
			break;
		case THREE_OF_A_KIND: {
			tiebreaker = computeTieBreaker(3l, h1, h2);
		}
			break;

		case STRAIGHT: {
			tiebreaker = computeTieBreaker(0l, h1, h2);
		}
			break;

		case FLUSH: {
			tiebreaker = computeTieBreaker(0l, h1, h2);
		}
			break;
		case FULL_HOUSE: {
			tiebreaker = computeTieBreaker(3l, h1, h2);
		}
			break;

		case FOUR_OF_A_KIND: {
			tiebreaker = computeTieBreaker(4l, h1, h2);
		}
			break;
		case STRAIGHT_FLUSH: {
			tiebreaker = computeTieBreaker(0l, h1, h2);
		}
			break;
		case ROYAL_FLUSH: {
			return null; // no way we can work out a winner here; quit now
		}
		default:
			tiebreaker = computeTieBreaker(0l, h1, h2);
			break;
		}

		// get the player with the max value of face card
		return tiebreaker.entrySet().stream().max(Map.Entry.comparingByValue()).get();
	}

	/**
	 * Calculate the rank to resolve the tiebreak
	 * 
	 * @param cardGroups - the number of face groups to check in the event of a
	 *                   <code>PAIR<code>, <code>TWO_PAIRS<code>,
	 *                   <code>THREE_OF_A_KIND<code>, <code>FLUSH<code>,
	 *                   <code>FULL_HOUSE<code>, <code>FOUR_OF_A_KIND<code>. If the
	 *                   rank is of a different type, simply check for the highest
	 *                   value card.
	 * @param h1
	 * @param h2
	 * @return - the score/rank of each player after the tie-break.
	 */
	private Map<Player, Integer> computeTieBreaker(final long cardGroups, final Hand h1, final Hand h2) {
		Map<Player, Integer> tiebreaker = new HashMap<>();
		Supplier<Stream<Entry<Face, Long>>> h1Faces = () -> faceGrouping.get(h1).entrySet().parallelStream();
		Supplier<Stream<Entry<Face, Long>>> h2Faces = () -> faceGrouping.get(h2).entrySet().parallelStream();

		Face face1 = null;
		Face face2 = null;

		if (cardGroups > 0) {
			// retrieve the card values; get last group (this is for two pairs, but every
			// other grouping will only have a single occurrence).
			face1 = h1Faces.get().filter(k -> Objects.equals(k.getValue(), cardGroups)).map(Map.Entry::getKey)
					.reduce((a, b) -> b).get();
			face2 = h2Faces.get().filter(k -> Objects.equals(k.getValue(), cardGroups)).map(Map.Entry::getKey)
					.reduce((a, b) -> b).get();
		}

		if ((face1 == null && face2 == null) || (face1.getValue() == face2.getValue())) {
			// tie again! get highest card.
			int highcard1 = minMax.get(h1).get("MAX");
			int highcard2 = minMax.get(h2).get("MAX");

			tiebreaker.put(playerONE, highcard1);
			tiebreaker.put(playerTWO, highcard2);

			if (highcard1 == highcard2) {
				// another tie; look further
				// last recourse is to check all high cards individually until we have a clear
				// high card for one player.
				List<Integer> h1faceValues = faceGrouping.get(h1).entrySet().stream().map(e -> e.getKey().getValue())
						.sorted(Comparator.reverseOrder()).collect(Collectors.toList());
				List<Integer> h2faceValues = faceGrouping.get(h2).entrySet().stream().map(e -> e.getKey().getValue())
						.sorted(Comparator.reverseOrder()).collect(Collectors.toList());
				for (int i = 1; i <= 4; i++) {
					if (h1faceValues.get(i) != h2faceValues.get(i)) {
						tiebreaker.put(playerONE, h1faceValues.get(i));
						tiebreaker.put(playerTWO, h2faceValues.get(i));
						break;
					}
					continue;
				}

			}

		} else {
			tiebreaker.put(playerONE, face1.getValue());
			tiebreaker.put(playerTWO, face2.getValue());
		}

		return tiebreaker;
	}

	/**
	 * Evaluate the given hand and return a rank
	 * 
	 * @param h - a hand of cards
	 * @return a rank based on card combinations. @see HandRank
	 */
	private HandRank evaluate(Hand h) {
		if (h == null || h.getCards().isEmpty() || h.getCards().size() != 5) {
			throw new IllegalStateException("Bad hand; cannot play!");
		}

		if (isRoyalFlush(h)) {
			return HandRank.ROYAL_FLUSH;
		}
		if (isStraightFlush(h)) {
			return HandRank.STRAIGHT_FLUSH;
		}
		if (isFourofaKind(h)) {
			return HandRank.FOUR_OF_A_KIND;
		}
		if (isFullHouse(h)) {
			return HandRank.FULL_HOUSE;
		}
		if (isFlush(h)) {
			return HandRank.FLUSH;
		}
		if (isStraight(h)) {
			return HandRank.STRAIGHT;
		}
		if (isThreeofaKind(h)) {
			return HandRank.THREE_OF_A_KIND;
		}
		if (isTwoPairs(h)) {
			return HandRank.TWO_PAIRS;
		}
		if (isPair(h)) {
			return HandRank.PAIR;
		}
		return HandRank.HIGH_CARD;
	}

	private boolean isPair(Hand h) {
		return areSame(faceValues(h), DealersAssistant.PAIR);
	}

	private boolean isTwoPairs(Hand h) {
		return areSame(faceValues(h), DealersAssistant.TWO_PAIRS);
	}

	private boolean isThreeofaKind(Hand h) {
		return areSame(faceValues(h), DealersAssistant.THREE_OF_A_KIND);
	}

	/**
	 * Check for a straight hand - basing this on the difference between the highest
	 * and lowest value cards being <code>equal</code> to 4.
	 * 
	 * @param h
	 * @return
	 */
	private boolean isStraight(Hand h) {
		// max - min of cards should be equal to 4
		return (minMax.get(h).get("MAX") - minMax.get(h).get("MIN") == 4)
				&& areSame(faceValues(h), DealersAssistant.SINGLES);
	}

	private boolean isFlush(Hand h) {
		return areSame(faceValues(h), DealersAssistant.FLUSH);
	}

	private boolean isFullHouse(Hand h) {
		return areSame(faceValues(h), DealersAssistant.FULL_HOUSE);
	}

	private boolean isFourofaKind(Hand h) {
		return areSame(faceValues(h), DealersAssistant.FOUR_OF_A_KIND);
	}

	private boolean isStraightFlush(Hand h) {
		return isStraight(h) && isFlush(h);
	}

	private boolean isRoyalFlush(Hand h) {
		Set<Card.Face> faces = faceGrouping.get(h).keySet();
		return (isStraightFlush(h) && areSame(faces, DealersAssistant.FACE_ROYAL_FLUSH));
	}

	/**
	 * Group the list of values of the unique (by value) cards in hand
	 * 
	 * @param h
	 * @return
	 */
	private Collection<Long> faceValues(Hand h) {
		return faceGrouping.get(h).values();
	}

	/**
	 * Check if both collections are the same in values
	 * 
	 * @param one
	 * @param two
	 * @return
	 */
	private <T extends Comparable<? super T>> boolean areSame(Collection<T> one, Collection<T> two) {
		if (one == null && two == null) {
			return true;
		}

		if ((one == null && two != null) || one != null && two == null || one.size() != two.size()) {
			return false;
		}

		List<T> l1 = new ArrayList<T>(one);
		List<T> l2 = new ArrayList<T>(two);

		Collections.sort(l1);
		Collections.sort(l2);
		return l1.equals(l2);
	}

	/**
	 * A handy utility to match groups of face values in each hand - for instance,
	 * the grouping of faces in a hand such as a full house will/must include
	 * <i>3</i> cards of a kind and <i>2</i> of another.
	 * 
	 * @author anandjayaraman
	 *
	 */
	static class DealersAssistant {
		static final List<Long> SINGLES = Collections.unmodifiableList(Arrays.asList(1l, 1l, 1l, 1l, 1l));
		static final List<Long> PAIR = Collections.unmodifiableList(Arrays.asList(1l, 1l, 1l, 2l));
		static final List<Long> TWO_PAIRS = Collections.unmodifiableList(Arrays.asList(1l, 2l, 2l));
		static final List<Long> THREE_OF_A_KIND = Collections.unmodifiableList(Arrays.asList(1l, 1l, 3l));
		static final List<Long> FLUSH = Collections.unmodifiableList(Arrays.asList(5l));
		static final List<Long> FULL_HOUSE = Collections.unmodifiableList(Arrays.asList(2l, 3l));
		static final List<Long> FOUR_OF_A_KIND = Collections.unmodifiableList(Arrays.asList(1l, 4l));

		static final List<Card.Face> FACE_ROYAL_FLUSH = Collections.unmodifiableList(
				Arrays.asList(Card.Face.ACE, Card.Face.KING, Card.Face.QUEEN, Card.Face.JACK, Card.Face.TEN));

	}
}
