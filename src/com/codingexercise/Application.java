package com.codingexercise;

import java.util.Scanner;

/**
 * The entrypoint to the poker hands evaluator application.
 * 
 * @author anandjayaraman
 *
 */
public class Application {

	public static void main(String[] args) {
		Application app = new Application();
		app.run();
	}

	public void run() {
		Scanner scan;
		try {
//			scan = new Scanner(getClass().getResourceAsStream("test.txt"));
			scan = new Scanner(System.in);

			Dealer dealer = Dealer.get();
			while (scan.hasNextLine()) {
				String game = scan.nextLine();

				dealer.deal(game);

				dealer.show();
			}
			scan.close();
			dealer.closeGame();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
