package com.codingexercise;

/**
 * A representation of each player of the game.
 * 
 * @author anandjayaraman
 *
 */
public final class Player {
	private final int id;
	private Hand hand;

	public Player(int id) {
		this.id = id;
	}

	public void hand(String hand) {
		this.setHand(Hand.of(hand));
	}

	public Hand getHand() {
		return hand;
	}

	public void setHand(Hand hand) {
		this.hand = hand;
	}

	public String toString() {
		return String.format("%s %d", "Player", id);
	}
}
