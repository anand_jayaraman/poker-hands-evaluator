package com.codingexercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * An individual hand (set of cards)
 * 
 * @author anandjayaraman
 *
 */
public final class Hand {

	private static final int HAND_STRING_FIXED_LENGTH = 14;

	private final Set<Card> cards;

	private Hand(List<Card> c) {
		this.cards = Collections.unmodifiableSet(new TreeSet<>(c));
	}

	/**
	 * Convert a string representation of a hand into a object associated with a set
	 * of cards
	 * 
	 * @param hand - a string representation of cards in the hand. This follows a format similar to 
	 * @return
	 */
	static Hand of(String hand) {
		if (!(hand.length() == HAND_STRING_FIXED_LENGTH))
			throw new IllegalStateException("Invalid hand; ");
		List<Card> cards = new ArrayList<>();
		cards = Arrays.asList(hand.split(" ")).parallelStream().map(c -> Card.of(c)).collect(Collectors.toList());

		return new Hand(cards);
	}

	public Set<Card> getCards() {
		return this.cards;
	}

}