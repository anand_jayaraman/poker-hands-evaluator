package com.codingexercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Card implements Comparable<Card> {

	/**
	 * The suit of a card - values are
	 * <code>C (Clubs), D (Diamonds), H (Hearts) and S (Spades)</code>
	 * 
	 * @author anandjayaraman
	 *
	 */
	public enum Suit {
	C, // CLUBS
	D, // DIAMONDS
	H, // Hearts
	S // Spades
	}

	/**
	 * The face or value or rank of a card.
	 * 
	 * NOTE: An ACE (A) is only considered as a high card and NOT as a 1 (low).
	 * 
	 * @author anandjayaraman
	 *
	 */
	public enum Face {
		TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), JACK(11), QUEEN(12), KING(13),
		ACE(14);

		private final int value;
		private final char notation;

		private Face(int value) {
			this.value = value;
			switch (value) {
			case 10:
				this.notation = 'T';
				break;
			case 11:
				this.notation = 'J';
				break;
			case 12:
				this.notation = 'Q';
				break;
			case 13:
				this.notation = 'K';
				break;
			case 14:
				this.notation = 'A';
				break;

			default:
				this.notation = (char) ('0' + value);
				break;
			}

		}

		private static final Map<Character, Face> reverselookup = new HashMap<>();
		static {
			// Create reverse lookup hash map
			for (Face f : Face.values()) {
				reverselookup.put(f.notation, f);
			}
		}

		/**
		 * Lookup the face of the card given its notation
		 * 
		 * @param f the character notation of a card
		 * @return the face of the card
		 */
		private static Face of(char f) {
			return reverselookup.get(f);
		}

		public int getValue() {
			return value;
		}
	}

	private final Face face;
	private final Suit suit;

	public Card(Face face, Suit suit) {
		this.face = face;
		this.suit = suit;
	}

	/**
	 * Return a card object matching the passed string (string MUST be in format of
	 * <code>fs</code>); for e.g.: <code>KD</code> will become: face - K/KING; and
	 * suit - D/Diamonds.
	 * 
	 * @param s
	 * @return
	 */
	public static Card of(String s) {
		char face = s.charAt(0);
		String suit = Character.toString(s.charAt(1));

		return new Card(Face.of(face), Suit.valueOf(suit));
	}

	public Face getFace() {
		return face;
	}

	public Suit getSuit() {
		return suit;
	}

	@Override
	public String toString() {
		return face.notation + "" + suit;
	}

	@Override
	public int compareTo(Card arg0) {
		int result = face.compareTo(arg0.face);
		if (result == 0) {
			return suit.compareTo(arg0.suit);
		}
		return result;
	}

	@Override
	public int hashCode() {
		return Objects.hash(face, suit);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof Card)) {
			return false;
		}
		Card card = (Card) o;
		return Objects.equals(face, card.face) && Objects.equals(face.getValue(), card.face.getValue())
				&& Objects.equals(suit, card.suit);
	}

}
